import React from 'react';

import CreateTaskForm from './containers/createTaskForm';
import TaskTable from './containers/tasksTable';
import Login from './containers/login';

import './App.css';

function App() {
  return (
    <div className="App">
      <CreateTaskForm />
      <Login />
      <TaskTable />
    </div>
  );
}

export default App;
