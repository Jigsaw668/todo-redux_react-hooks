export const GET_TASKS = 'GET_TASKS';
export const GET_PAGE = 'GET_PAGE';
export const TOGGLE_SORT = 'TOGGLE_SORT';

export const OPEN_CREATE = 'OPEN_CREATE';

export const OPEN_LOGIN = 'OPEN_LOGIN';
export const GET_TOKEN = 'GET_TOKEN';
export const REMOVE_TOKEN = 'REMOVE_TOKEN';

export const OPEN_EDIT = 'OPEN_EDIT';
