import {
  OPEN_CREATE
} from '../constans';

export function isOpenCreate(bool) {
  return (dispatch) => {
    dispatch({
      type: OPEN_CREATE,
      payload: bool
    });
  };
};