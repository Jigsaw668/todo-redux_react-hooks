import {
  OPEN_LOGIN,
  GET_TOKEN,
  REMOVE_TOKEN
} from '../constans';

export function isOpenLogin(bool) {
  return (dispatch) => {
    dispatch({
      type: OPEN_LOGIN,
      payload: bool
    });
  };
};

export function getToken(token) {
  localStorage.setItem('token', token);

  return (dispatch) => {
    dispatch({
      type: GET_TOKEN,
      payload: token
    });
  };
};

export function removeToken() {
  localStorage.setItem('token', '');

  return (dispatch) => {
    dispatch({
      type: REMOVE_TOKEN,
      payload: ''
    });
  };
};
