import {
  OPEN_EDIT
} from '../constans';

export function isOpenEdit(bool) {
  return (dispatch) => {
    dispatch({
      type: OPEN_EDIT,
      payload: bool
    });
  };
};
