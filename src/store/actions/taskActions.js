import {
  GET_TASKS,
  GET_PAGE,
  TOGGLE_SORT
} from '../constans';

export function getTasks(tasks) {
  return (dispatch) => {
    dispatch({
      type: GET_TASKS,
      payload: tasks
    });
  };
};

export function getPage(page) {
  return (dispatch) => {
    dispatch({
      type: GET_PAGE,
      payload: page
    });
  };
};

export function toggleSort(sort) {
  return (dispatch) => {
    dispatch({
      type: TOGGLE_SORT,
      payload: sort
    });
  };
};