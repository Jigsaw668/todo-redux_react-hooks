import {
  OPEN_LOGIN,
  GET_TOKEN,
  REMOVE_TOKEN
} from '../constans';

export const initialState = {
  isOpenLogin: false,
  token: window.localStorage.getItem('token') || ''
};

export default function loginReducer(state = initialState, action) {
  switch (action.type) {
    case OPEN_LOGIN:
      return {
        ...state,
        isOpenLogin: action.payload
      };
    case GET_TOKEN:
      return {
        ...state,
        token: action.payload
      };
    case REMOVE_TOKEN:
      return {
        ...state,
        token: action.payload
      };

    default:
      return state;
  }
};
