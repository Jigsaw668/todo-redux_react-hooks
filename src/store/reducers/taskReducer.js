import {
  GET_TASKS,
  GET_PAGE,
  TOGGLE_SORT
} from '../constans';

export const initialState = {
  tasks: [],
  currentPage: 1,
  totalCount: 0,
  sort: {
    sort_field: '',
    sort_direction: ''
  }
};

export default function taskReducer(state = initialState, action) {
  switch (action.type) {
    case GET_TASKS:
      return {
        ...state,
        tasks: action.payload,
        totalCount: action.payload.message.total_task_count
      };
    case GET_PAGE:
      return {
        ...state,
        currentPage: action.payload
      };
    case TOGGLE_SORT:
      return {
        ...state,
        sort: action.payload
      }

    default:
      return state;
  }
};
