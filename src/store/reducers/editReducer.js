import {
  OPEN_EDIT
} from '../constans';

export const initialState = {
  isOpenEdit: false,
};

export default function editReducer(state = initialState, action) {
  switch (action.type) {
  case OPEN_EDIT:
      return {
        ...state,
        isOpenEdit: action.payload
      };

    default:
      return state;
  }
};
