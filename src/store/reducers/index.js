import { combineReducers } from 'redux';

import taskReducer from './taskReducer';
import createTaskReducer from './createTaskReducer';
import LoginReducer from './LoginReducer';
import EditReducer from './editReducer';

export default combineReducers({
  taskReducer,
  createTaskReducer,
  LoginReducer,
  EditReducer
});
