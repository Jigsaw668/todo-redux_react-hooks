import {
  OPEN_CREATE,
} from '../constans';

export const initialState = {
  isOpenCreate: false
};

export default function taskReducer(state = initialState, action) {
  switch (action.type) {
    case OPEN_CREATE:
      return {
        ...state,
        isOpenCreate: action.payload
      };

    default:
      return state;
  }
};
