import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import App from './App';

import './index.css';

import configureStore from './store/store';

const store = configureStore();

render(
  <Provider store={store}>
    <div>
      <App />
    </div>
  </Provider>,
  document.getElementById('root')
);
