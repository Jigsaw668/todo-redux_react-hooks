import React, { useState } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';

import * as createTaskActions from '../../store/actions/createTaskActions';
import * as taskActions from '../../store/actions/taskActions';

import './CreateTaskForm.scss';

const CreateTaskForm = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [task, setTask] = useState('');

  const currentPage = useSelector(state => state.taskReducer.currentPage, shallowEqual);
  const currentSort = useSelector(state => state.taskReducer.sort, shallowEqual);

  const sortField = currentSort.sort_field || '';
  const sortDirection = currentSort.sort_direction || '';

  const isOpen = useSelector(state => state.createTaskReducer.isOpenCreate, shallowEqual);

  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append('username', name);
    formData.append('email', email);
    formData.append('text', task);

    fetch(`https://uxcandy.com/~shapoval/test-task-backend/v2/create?developer=Denis`, {
      method: 'POST',
      body: formData,
    })
      .then(() => dispatch(createTaskActions.isOpenCreate(false)))
      .then(() => alert('Задача успешно создана'))
      .then(() => {
        setName('');
        setEmail('');
        setTask('');
      })
      .then(() => {
        fetch(`https://uxcandy.com/~shapoval/test-task-backend/v2/?developer=Denis&sort_field=${sortField}&sort_direction=${sortDirection}&page=${currentPage}`, {
          method: 'GET',
        })
          .then((res) => res.json())
          .then((res) => dispatch(taskActions.getTasks(res)))
      })
  };

  return (
    <>
      <button onClick={() => dispatch(createTaskActions.isOpenCreate(isOpen ? false : true))}>Создать задачу</button>
      { isOpen &&
        <div className='popup-wrapp'>
          <div className='popup-overlay' onClick={() => dispatch(createTaskActions.isOpenCreate(false))} />
          <div className='popup-content'>
            <h2>Создание задачи</h2>
            <div className='top'>
                <button onClick={() => dispatch(createTaskActions.isOpenCreate(false))}>x</button>
            </div>
            <form onSubmit={(e) => handleSubmit(e)}>
              <label>
                Имя пользователя
                <input type='text' name='username' value={name} onChange={e => setName(e.target.value)} required></input>
              </label>
              <label>
                E-mail
                <input type='email' name='email' value={email} onChange={e => setEmail(e.target.value)} required></input>
              </label>
              <label>
                Текст задачи
                <textarea value={task} onChange={e => setTask(e.target.value)} required></textarea>
              </label>
              <input type='submit'></input>
            </form>
          </div>
        </div>
      }
    </>
  );
};

export default CreateTaskForm;
