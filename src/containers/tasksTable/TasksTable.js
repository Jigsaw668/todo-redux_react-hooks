import React, { useEffect } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';

import Pagination from "react-js-pagination";

import * as taskActions from '../../store/actions/taskActions';
import * as editActions from '../../store/actions/editActions';

import './TasksTable.scss';

const tableRow = (key, token, isOpenEdit, dispatch, sortField, sortDirection, currentPage, ...args) => {
  return (
    <tr key={key}>
      <td>{args[0].username}</td>
      <td>{args[0].email}</td>
      <td>{args[0].text}</td>
      <td>{args[0].status}</td>
      { token !== '' &&
        <td>
          <button name={`${args[0].id}`} onClick={() => dispatch(editActions.isOpenEdit(true))}/>
          { isOpenEdit &&
            <EditPopup
              isOpenEdit={isOpenEdit}
              token={token}
              dispatch={dispatch}
              id={args[0].id}
              text={args[0].text}
              status={args[0].status}
              sortField={sortField}
              sortDirection={sortDirection}
              currentPage={currentPage}
            />
          }
        </td>
      }
    </tr>
  );
};

class EditPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: this.props.text,
      status: this.props.status
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit = (e) => {
    e.preventDefault();

    function editAdmin(text) {
      if (text.indexOf('(отредактировано администратором)') + 1 !== 0) {
        return text;
      } else {
        return text + ' (отредактировано администратором)';
      }
    }

    const data = new FormData();

    data.append('token', this.props.token);
    data.append('text', `${this.state.text === this.props.text ? this.props.text : editAdmin(this.state.text)}`);
    data.append('status', this.state.status);

    fetch(`https://uxcandy.com/~shapoval/test-task-backend/v2/edit/${this.props.id}?developer=Denis`, {
      method: 'POST',
      body: data,
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res)
        if (res.status !== 'error') {
          return true
        } else {
          return false
        }
      })
      .then((bool) => {
        if (bool) {
          alert('ok');
          return true;
        } else {
          alert('Пожалуйста, авторизуйтесь')
          window.localStorage.setItem('token', '');
        }
      })
      .then((bool) => {
        if (bool) {
          fetch(`https://uxcandy.com/~shapoval/test-task-backend/v2/?developer=Denis&sort_field=${this.props.sortField}&sort_direction=${this.props.sortDirection}&page=${this.props.currentPage}`, {
            method: 'GET',
          })
            .then((res) => res.json())
            .then((res) => this.props.dispatch(taskActions.getTasks(res)))
        }
      })
      .then(() => this.props.dispatch(editActions.isOpenEdit(false)));
  };

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <>
        { this.props.isOpenEdit &&
          <div className='popup-wrapp'>
            <div className='popup-overlay' onClick={() => this.props.dispatch(editActions.isOpenEdit(false))} />
            <div className='popup-content' style={{left: 'calc(50% - 306px)'}}>
              <h2>Редактирование задачи</h2>
              <div className='top'>
                  <button style={{backgroundImage: 'none'}} onClick={() => this.props.dispatch(editActions.isOpenEdit(false))}>x</button>
              </div>
              <form onSubmit={this.handleSubmit}>
                <label>
                  Текст
                  <input type='text' name='text' value={this.state.text} onChange={this.handleInputChange} required></input>
                </label>
                <label>
                  Статус
                  <input type='number' min='0' max='10' step='10' name='status' value={this.state.status} onChange={this.handleInputChange} required></input>
                </label>
                <input type='submit'></input>
              </form>
            </div>
          </div>
        }
      </>
    );
  };
};

const sortButtons = (sort, dispatch) => {
  return (
    <>
      <button name={`${sort}-asc`} onClick={() => dispatch(taskActions.toggleSort({sort_field: sort, sort_direction: 'asc'}))} />
      <button name={`${sort}-desc`} onClick={() => dispatch(taskActions.toggleSort({sort_field: sort, sort_direction: 'desc'}))}/>
    </>
  );
};

const TasksTable = () => {
  const taskList = useSelector(state => state.taskReducer.tasks.message, shallowEqual);
  const currentPage = useSelector(state => state.taskReducer.currentPage, shallowEqual);
  const totalTask = useSelector(state => state.taskReducer.totalCount, shallowEqual);
  const currentSort = useSelector(state => state.taskReducer.sort, shallowEqual);

  const token = useSelector(state => state.LoginReducer.token, shallowEqual);
  const isOpenEdit = useSelector(state => state.EditReducer.isOpenEdit, shallowEqual);

  const sortField = currentSort.sort_field || '';
  const sortDirection = currentSort.sort_direction || '';

  const dispatch = useDispatch();

  useEffect(() => {
    fetch(`https://uxcandy.com/~shapoval/test-task-backend/v2/?developer=Denis&sort_field=${sortField}&sort_direction=${sortDirection}&page=${currentPage}`, {
      method: 'GET',
    })
      .then((res) => res.json())
      .then((res) => dispatch(taskActions.getTasks(res)))
  }, [dispatch, currentPage, sortField, sortDirection]);

  return (
    <>
      <table>
        <thead>
          <tr>
            <th>
              имя пользователя
              {sortButtons('name', dispatch)}
            </th>
            <th>
              email
              {sortButtons('email', dispatch)}
            </th>
            <th>текст задачи</th>
            <th>
              статус
              {sortButtons('status', dispatch)}
            </th>
            { token !== '' &&
              <th>Edit</th>
            }
          </tr>
        </thead>
        <tbody>
          { taskList &&
            taskList.tasks.map((task, key) => {
              return (
                tableRow(key, token, isOpenEdit, dispatch, sortField, sortDirection, currentPage, task)
              );
            })
          }
        </tbody>
      </table>
      <div>
        <Pagination
          activePage={currentPage}
          itemsCountPerPage={3}
          totalItemsCount={totalTask}
          pageRangeDisplayed={5}
          onChange={(n) => dispatch(taskActions.getPage(n))}
        />
      </div>
    </>
  );
};

export default TasksTable;
