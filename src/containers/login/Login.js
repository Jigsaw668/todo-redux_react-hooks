import React, { useState } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';

import * as loginActions from '../../store/actions/loginActions';

import './Login.scss';

const Login = () => {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');

  const isOpen = useSelector(state => state.LoginReducer.isOpenLogin, shallowEqual);
  const token = useSelector(state => state.LoginReducer.token, shallowEqual);

  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();

    const data = new FormData();

    data.append('username', name);
    data.append('password', password);

    fetch(`https://uxcandy.com/~shapoval/test-task-backend/v2/login?developer=Denis`, {
      method: 'POST',
      body: data,
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.status !== 'error') {
          alert('Авторизация пройдена')
        } else {
          alert('Авторизация не пройдена')
        }
        return res;
      })
      .then((res) => dispatch(loginActions.getToken(res.message.token)))
      .then(() => dispatch(loginActions.isOpenLogin(false)))
  };

  return (
    <>
      { token === '' &&
        <button className='auth' onClick={() => dispatch(loginActions.isOpenLogin(isOpen ? false : true))}>Вход в админку</button>
      }
      { token !== '' &&
        <button className='auth' onClick={() => dispatch(loginActions.removeToken())}>Выйти из админки</button>
      }
      { isOpen &&
        <div className='popup-wrapp'>
          <div className='popup-overlay' onClick={() => dispatch(loginActions.isOpenLogin(false))} />
          <div className='popup-content'>
            <h2>Авторизация</h2>
            <div className='top'>
                <button onClick={() => dispatch(loginActions.isOpenLogin(false))}>x</button>
            </div>
            <form onSubmit={(e) => handleSubmit(e)}>
              <label>
                Имя пользователя
                <input type='text' name='username' value={name} onChange={e => setName(e.target.value)} required></input>
              </label>
              <label>
                Пароль
                <input type='password' name='email' value={password} onChange={e => setPassword(e.target.value)} required></input>
              </label>
              <input type='submit'></input>
            </form>
          </div>
        </div>
      }
    </>
  );
};

export default Login;
